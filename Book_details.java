package bookstore_system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;

import net.proteanit.sql.DbUtils;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Book_details extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Book_details frame = new Book_details();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
Connection conn=null;
private JTable table;
private JTextField search;
	/**
	 * Create the frame.
	 */
	public Book_details() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "keeran");
		} catch (Exception e) {
			{
				System.out.println(e);
			}

		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 602, 432);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 0, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnviewButton = new JButton("View All Book Details");
		btnviewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					String sql = "select * from book_data";
					PreparedStatement pst=conn.prepareStatement(sql);
					ResultSet rs=pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnviewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnviewButton.setForeground(new Color(0, 0, 0));
		btnviewButton.setBackground(new Color(0, 255, 153));
		btnviewButton.setBounds(10, 107, 171, 23);
		contentPane.add(btnviewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 162, 566, 192);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Book Id", "Book Name", "Author Name", "Price", "Quantity"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, Float.class, Integer.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		scrollPane.setColumnHeaderView(table);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Book_Search hframe=new Book_Search();
					hframe.setVisible(true);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnNewButton.setForeground(new Color(0, 0, 0));
		btnNewButton.setBackground(new Color(0, 255, 153));
		btnNewButton.setBounds(487, 365, 89, 23);
		contentPane.add(btnNewButton);
		
		search = new JTextField();
		search.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				
				try {
					String query="select * from book_data where book_name=?";
					PreparedStatement pst=conn.prepareStatement(query);
					pst.setString(1, search.getText());
					ResultSet rs=pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
					/*while(rs.next())
					{
						
					}*/
					pst.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		search.setBounds(410, 109, 146, 20);
		contentPane.add(search);
		search.setColumns(10);
		
		JLabel lblSearchName = new JLabel("Search Book Name");
		lblSearchName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblSearchName.setForeground(new Color(255, 255, 255));
		lblSearchName.setBounds(254, 112, 146, 14);
		contentPane.add(lblSearchName);
		
		JLabel lblNewLabel = new JLabel("Book Details");
		lblNewLabel.setForeground(new Color(0, 255, 0));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNewLabel.setBounds(191, 26, 171, 23);
		contentPane.add(lblNewLabel);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Logout hframe=new Logout();
					hframe.setVisible(true);
			}
		});
		btnClose.setForeground(Color.BLACK);
		btnClose.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnClose.setBackground(new Color(0, 255, 153));
		btnClose.setBounds(388, 366, 89, 23);
		contentPane.add(btnClose);
	}
}
