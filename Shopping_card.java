package bookstore_system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Shopping_card extends JFrame {

	private JPanel contentPane;
	private JTextField ui;
	private JTextField cn;
	
	public JLabel lbla;
	private JTextField amount;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Shopping_card frame = new Shopping_card();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	/*public String[] getdata=new String[1];
	private JTextField amount;
	public void postData() {
		amount.setVisible(true);
		amount.setText("lblAmount:"+getdata[0]);
	}*/
	public Shopping_card() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 304, 300);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(255, 0, 0));
		contentPane.setBackground(new Color(0, 191, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Payment");
		lblNewLabel.setForeground(new Color(255, 0, 0));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblNewLabel.setBounds(94, 19, 100, 25);
		contentPane.add(lblNewLabel);
		
		JLabel lblUserId = new JLabel("User ID:");
		lblUserId.setForeground(new Color(255, 0, 0));
		lblUserId.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblUserId.setBounds(28, 55, 73, 14);
		contentPane.add(lblUserId);
		
		JLabel lblCardNo = new JLabel("Card No:");
		lblCardNo.setForeground(new Color(255, 0, 0));
		lblCardNo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblCardNo.setBounds(28, 92, 73, 14);
		contentPane.add(lblCardNo);
		
		JLabel lblAmount = new JLabel("Amount:");
		lblAmount.setForeground(new Color(255, 0, 0));
		lblAmount.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblAmount.setBounds(28, 130, 73, 14);
		contentPane.add(lblAmount);
		
		ui = new JTextField();
		ui.setBounds(108, 55, 114, 20);
		contentPane.add(ui);
		ui.setColumns(10);
		
		cn = new JTextField();
		cn.setColumns(10);
		cn.setBounds(108, 92, 114, 20);
		contentPane.add(cn);
		
		JButton btnpay = new JButton("Pay");
		btnpay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(ui.getText().trim().isEmpty()&&cn.getText().trim().isEmpty()&&amount.getText().trim().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Please Enter information");
				}else if(ui.getText().trim().isEmpty()){
					JOptionPane.showMessageDialog(null, "Please Enter user id");
				}else if(cn.getText().trim().isEmpty()){
					JOptionPane.showMessageDialog(null, "Please Enter user card no");
				}else if(amount.getText().trim().isEmpty()){
					JOptionPane.showMessageDialog(null, "Please Enter Total amount");
				}else
				{JOptionPane.showMessageDialog(null, "paid Successfully");
					dispose();
					Details ds=new Details();
					ds.id.setText(ui.getText());
					ds.ta.setText(amount.getText());
					ds.setVisible(true);
				}
				
				
			}
		});
		btnpay.setForeground(new Color(255, 255, 255));
		btnpay.setBackground(new Color(50, 205, 50));
		btnpay.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnpay.setBounds(12, 202, 89, 23);
		contentPane.add(btnpay);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ui.setText(null);
				cn.setText(null);
				amount.setText(null);
			}
		});
		btnClear.setBackground(new Color(255, 255, 255));
		btnClear.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnClear.setBounds(163, 202, 89, 23);
		contentPane.add(btnClear);
		
		amount = new JTextField();
		amount.setColumns(10);
		amount.setBounds(108, 130, 114, 20);
		contentPane.add(amount);
	}

	
}
