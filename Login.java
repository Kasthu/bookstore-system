package bookstore_system;

import java.sql.*;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class Login {

	private JFrame frame;
	public JTextField uname;
	public JPasswordField pw;
	public JTextField uid;

	/**
	 * Launch the application.
	 */

	public static void Newscreen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(
				Toolkit.getDefaultToolkit().getImage("C:\\Users\\admin\\Desktop\\ap\\image\\HessBookstore_pic1.jpg"));
		frame.getContentPane().setBackground(new Color(204, 51, 153));
		frame.setBounds(100, 100, 453, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblUserName = new JLabel("User name");
		lblUserName.setFont(new Font("Franklin Gothic Book", Font.BOLD, 17));
		lblUserName.setForeground(Color.WHITE);
		lblUserName.setBounds(36, 104, 100, 14);
		frame.getContentPane().add(lblUserName);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Franklin Gothic Book", Font.BOLD, 18));
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setBounds(36, 141, 88, 14);
		frame.getContentPane().add(lblPassword);

		uname = new JTextField();
		uname.setBounds(146, 103, 136, 20);
		frame.getContentPane().add(uname);
		uname.setColumns(10);

		pw = new JPasswordField();
		pw.setBounds(146, 141, 136, 20);
		frame.getContentPane().add(pw);

		JButton btnWorkerLogin = new JButton(" Worker Login");
		btnWorkerLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String UserID = uid.getText();
				String Username = uname.getText();
				String Password = pw.getText();

				if (UserID.equals("n01") && Username.equals("nj") && Password.equals("nj456")) {
					JOptionPane.showMessageDialog(frame, "You are successfully logined");
					
					frame.dispose();
					Home_Page hframe=new Home_Page();
					hframe.setVisible(true);
					
					
				} else {
					JOptionPane.showMessageDialog(frame, "Invalid userid or username or password");
					uid.setText(null);
					uname.setText(null);
					pw.setText(null);
				}
			}
		});
		btnWorkerLogin.setFont(new Font("Franklin Gothic Book", Font.BOLD, 14));
		btnWorkerLogin.setForeground(new Color(0, 0, 0));
		btnWorkerLogin.setBackground(new Color(0, 255, 153));
		btnWorkerLogin.setBounds(21, 179, 149, 23);
		frame.getContentPane().add(btnWorkerLogin);

		JButton btnCustomerLogin = new JButton("Customer Login");
		btnCustomerLogin.setFont(new Font("Franklin Gothic Book", Font.BOLD, 14));
		btnCustomerLogin.setForeground(new Color(0, 0, 0));
		btnCustomerLogin.setBackground(new Color(0, 255, 153));
		btnCustomerLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root",
							"keeran");
					Statement stmt = con.createStatement();
					String sql = "select * from user_register where id='" + uid.getText() + "' and uname='"
							+ uname.getText() + "' and pass='" + pw.getText().toString() + "'";
					ResultSet rs = stmt.executeQuery(sql);
					if (rs.next()) {
						JOptionPane.showMessageDialog(null, "You are successfully logined");
						frame.dispose();
						Home_Page hframe=new Home_Page();
						hframe.setVisible(true);
					
					}
					else
						JOptionPane.showMessageDialog(null, "Invalid userid or username or password");
					uid.setText(null);
					uname.setText(null);
					pw.setText(null);
					con.close();
				} catch (Exception e1) {
					System.out.print(e1);
				}
			}
		});

		/*
		 * String Username=uname.getText(); String Password=pw.getText();
		 * 
		 * if(Username.equals("name")&& Password.equals("password")) {
		 * JOptionPane.showMessageDialog(frame, "You are successfully logined"); } else
		 * { JOptionPane.showMessageDialog(frame, "Invalid username or password");
		 * uname.setText(null); pw.setText(null);
		 * 
		 * } } });
		 */
		btnCustomerLogin.setBounds(251, 179, 149, 23);
		frame.getContentPane().add(btnCustomerLogin);

		JLabel lblNewLabel = new JLabel("NJ Bookstore");
		lblNewLabel.setForeground(new Color(153, 255, 153));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 22));
		lblNewLabel.setBounds(151, 21, 221, 23);
		frame.getContentPane().add(lblNewLabel);

		JButton butcancel = new JButton("Cancel");
		butcancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		butcancel.setForeground(Color.BLACK);
		butcancel.setFont(new Font("Franklin Gothic Book", Font.BOLD, 14));
		butcancel.setBackground(Color.RED);
		butcancel.setBounds(21, 227, 149, 23);
		frame.getContentPane().add(butcancel);

		JLabel lblUserId = new JLabel("User ID");
		lblUserId.setForeground(Color.WHITE);
		lblUserId.setFont(new Font("Franklin Gothic Book", Font.BOLD, 17));
		lblUserId.setBounds(36, 69, 100, 14);
		frame.getContentPane().add(lblUserId);

		uid = new JTextField();
		uid.setColumns(10);
		uid.setBounds(146, 68, 136, 20);
		frame.getContentPane().add(uid);

		JButton refresh = new JButton("Refresh");
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				uid.setText(null);
				uname.setText(null);
				pw.setText(null);
			}
		});
		refresh.setForeground(Color.BLACK);
		refresh.setFont(new Font("Franklin Gothic Book", Font.BOLD, 14));
		refresh.setBackground(Color.RED);
		refresh.setBounds(251, 228, 149, 23);
		frame.getContentPane().add(refresh);
	}

	
}
