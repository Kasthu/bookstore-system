package bookstore_system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.awt.event.ActionEvent;

public class Details extends JFrame {

	private JPanel contentPane;
	public JTextField id;
	public JTextField ta;
	private JLabel lblclock;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Details frame = new Details();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
public void clock() {
	Calendar cal=new GregorianCalendar();
	int day=cal.get(Calendar.DAY_OF_MONTH);
	int month=cal.get(Calendar.MONTH);
	int year=cal.get(Calendar.YEAR);
	
	int second=cal.get(Calendar.SECOND);
	int minute=cal.get(Calendar.MINUTE);
	int hour=cal.get(Calendar.HOUR);
	lblclock.setText(" Date"+year+"/"+month+"/"+day+"    "+"Time"+hour+":"+minute+":"+second);
	
}

	/**
	 * Create the frame.
	 */
	public Details() {
		//clock();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 317, 441);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(128, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNjBookstore = new JLabel("NJ BookStore");
		lblNjBookstore.setForeground(new Color(255, 255, 0));
		lblNjBookstore.setFont(new Font("Tahoma", Font.BOLD, 17));
		lblNjBookstore.setBounds(83, 11, 137, 26);
		contentPane.add(lblNjBookstore);
		
		JLabel lblNewLabel = new JLabel("User ID");
		lblNewLabel.setForeground(new Color(255, 255, 0));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(23, 73, 77, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblTotalAmount = new JLabel("Total Amount");
		lblTotalAmount.setForeground(new Color(255, 255, 0));
		lblTotalAmount.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblTotalAmount.setBounds(23, 126, 114, 14);
		contentPane.add(lblTotalAmount);
		
		id = new JTextField();
		id.setBounds(156, 73, 97, 20);
		contentPane.add(id);
		id.setColumns(10);
		
		ta = new JTextField();
		ta.setColumns(10);
		ta.setBounds(156, 126, 97, 20);
		contentPane.add(ta);
		
		JButton btnok = new JButton("OK");
		btnok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Logout hframe=new Logout();
					hframe.setVisible(true);
			}
		});
		btnok.setBackground(new Color(255, 99, 71));
		btnok.setBounds(100, 314, 89, 23);
		contentPane.add(btnok);
		
		lblclock = new JLabel("Clock");
		lblclock.setForeground(new Color(255, 255, 0));
		lblclock.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblclock.setBounds(10, 170, 268, 72);
		contentPane.add(lblclock);
		clock();
	}

}
