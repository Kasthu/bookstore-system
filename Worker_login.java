package bookstore_system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class Worker_login extends JFrame {

	private JPanel contentPane;
	private JTextField uname;
	private JPasswordField pw;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Worker_login frame = new Worker_login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Worker_login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 327, 363);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label_1 = new JLabel("User name");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Franklin Gothic Book", Font.BOLD, 17));
		label_1.setBounds(10, 65, 100, 14);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Password");
		label_2.setForeground(Color.WHITE);
		label_2.setFont(new Font("Franklin Gothic Book", Font.BOLD, 18));
		label_2.setBounds(10, 102, 88, 14);
		contentPane.add(label_2);
		
		uname = new JTextField();
		uname.setColumns(10);
		uname.setBounds(120, 64, 136, 20);
		contentPane.add(uname);
		
		pw = new JPasswordField();
		pw.setBounds(120, 102, 136, 20);
		contentPane.add(pw);
		
		JButton btnlogin = new JButton(" Worker Login");
		btnlogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String Username=uname.getText();
				String Password=pw.getText();
				
				if(Username.equals("nj")&& Password.equals("nj456"))
				{
					JOptionPane.showMessageDialog(contentPane, "You are successfully logined");
					Book_data frame = null;
					try {
						frame = new Book_data();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					frame.setVisible(true);
					dispose();
				}
				else
				{
					JOptionPane.showMessageDialog(contentPane, "Invalid userid or username or password");
					
					uname.setText(null);
					pw.setText(null);
				}
			}
		});
			
		btnlogin.setForeground(Color.BLACK);
		btnlogin.setFont(new Font("Franklin Gothic Book", Font.BOLD, 14));
		btnlogin.setBackground(new Color(0, 255, 153));
		btnlogin.setBounds(10, 190, 141, 23);
		contentPane.add(btnlogin);
		
		JButton btncancel = new JButton("Cancel");
		btncancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btncancel.setForeground(Color.BLACK);
		btncancel.setFont(new Font("Franklin Gothic Book", Font.BOLD, 14));
		btncancel.setBackground(Color.RED);
		btncancel.setBounds(165, 190, 141, 23);
		contentPane.add(btncancel);
	}
}
