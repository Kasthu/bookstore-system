-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.15-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for bookstore
CREATE DATABASE IF NOT EXISTS `bookstore` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bookstore`;


-- Dumping structure for table bookstore.book_data
CREATE TABLE IF NOT EXISTS `book_data` (
  `book_id` int(11) NOT NULL,
  `book_name` varchar(45) DEFAULT NULL,
  `author_name` varchar(45) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bookstore.book_data: ~4 rows (approximately)
/*!40000 ALTER TABLE `book_data` DISABLE KEYS */;
INSERT INTO `book_data` (`book_id`, `book_name`, `author_name`, `price`, `quantity`) VALUES
	(1, 'TECHNOLOGY', 'P.Jeevan', 500, 15),
	(2, 'BASIC OF COMPUTER ', 'A.Sasikumar', 450, 12),
	(3, 'TECHNOLOGY', 'K.Raji', 550, 20),
	(4, 'SCIENCE', 'L.Jeeva', 400, 20);
/*!40000 ALTER TABLE `book_data` ENABLE KEYS */;


-- Dumping structure for table bookstore.user_register
CREATE TABLE IF NOT EXISTS `user_register` (
  `id` int(10) unsigned NOT NULL,
  `fname` varchar(20) DEFAULT NULL,
  `lname` varchar(20) DEFAULT NULL,
  `uname` varchar(20) DEFAULT NULL,
  `pass` varchar(10) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table bookstore.user_register: ~4 rows (approximately)
/*!40000 ALTER TABLE `user_register` DISABLE KEYS */;
INSERT INTO `user_register` (`id`, `fname`, `lname`, `uname`, `pass`, `address`) VALUES
	(1, 'Aruntha', 'Nakkeeran', 'aru@gmail.com', 'aru', 'Kilinochchi'),
	(2, 'Nilani', 'Siva', 'nila@gmail.com', 'nila', 'Jaffna'),
	(3, 'Sumathy', 'Keeran', 'mathy@gmail.com', 'mathy', 'Colombo'),
	(4, 'Sujatha', 'Sankar', 'suja@gmail.com', 'suja', 'Jaffna');
/*!40000 ALTER TABLE `user_register` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
