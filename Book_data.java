package bookstore_system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class Book_data extends JFrame {

	private JPanel contentPane;
	private JTextField b_id;
	private JTextField bn;
	private JTextField an;
	private JTextField pri;
	private JTextField qy;
	protected String insert;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Book_data frame = new Book_data();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection conn = null;

	public Book_data() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "keeran");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 296, 397);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(255, 0, 0));
		contentPane.setBackground(new Color(0, 0, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblBookId = new JLabel("Book Entry");
		lblBookId.setForeground(new Color(0, 191, 255));
		lblBookId.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblBookId.setBounds(87, 11, 93, 30);
		contentPane.add(lblBookId);

		JLabel lblBookName = new JLabel("Author Name");
		lblBookName.setForeground(new Color(255, 255, 255));
		lblBookName.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblBookName.setBounds(10, 115, 117, 14);
		contentPane.add(lblBookName);

		JLabel label = new JLabel("Book Name");
		label.setForeground(new Color(255, 255, 255));
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		label.setBounds(10, 82, 93, 14);
		contentPane.add(label);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setForeground(new Color(255, 255, 255));
		lblPrice.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblPrice.setBounds(10, 140, 93, 27);
		contentPane.add(lblPrice);

		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setForeground(new Color(255, 255, 255));
		lblQuantity.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblQuantity.setBounds(10, 178, 93, 16);
		contentPane.add(lblQuantity);

		b_id = new JTextField();
		b_id.setBounds(135, 52, 110, 14);
		contentPane.add(b_id);
		b_id.setColumns(10);

		bn = new JTextField();
		bn.setColumns(10);
		bn.setBounds(135, 84, 110, 14);
		contentPane.add(bn);

		an = new JTextField();
		an.setColumns(10);
		an.setBounds(135, 117, 110, 14);
		contentPane.add(an);

		pri = new JTextField();
		pri.setColumns(10);
		pri.setBounds(135, 150, 110, 14);
		contentPane.add(pri);

		qy = new JTextField();
		qy.setColumns(10);
		qy.setBounds(135, 180, 110, 14);
		contentPane.add(qy);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int book_id=Integer.parseInt(b_id.getText()); 
				 String book_name=bn.getText(); 
				 String author_name=an.getText();
				 float price=Float.parseFloat(pri.getText()); 
				 int quantity=Integer.parseInt(qy.getText());
				 int i=Bookdata_connect.save(book_id, book_name, author_name, price,  quantity);
					if(i>0) {
						JOptionPane.showMessageDialog(Book_data.this, "Add Book Successfully");
						
						b_id.setText(null);
						bn.setText(null);
						an.setText(null);
						pri.setText(null);
						qy.setText(null);
						
						
					}else{
						JOptionPane.showMessageDialog(Book_data.this,"Sorry, unable to Add!");
					}
			}
		});

		btnAdd.setForeground(new Color(255, 255, 255));
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnAdd.setBackground(new Color(255, 0, 0));
		btnAdd.setBounds(10, 249, 66, 23);
		contentPane.add(btnAdd);

		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				b_id.setText(null);
				bn.setText(null);
				an.setText(null);
				pri.setText(null);
				qy.setText(null);
				
			}
		});
		btnClear.setForeground(Color.WHITE);
		btnClear.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnClear.setBackground(Color.RED);
		btnClear.setBounds(87, 249, 77, 23);
		contentPane.add(btnClear);

		JLabel label_1 = new JLabel("Book ID");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_1.setBounds(10, 52, 93, 14);
		contentPane.add(label_1);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnCancel.setForeground(Color.WHITE);
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnCancel.setBackground(Color.RED);
		btnCancel.setBounds(174, 249, 96, 23);
		contentPane.add(btnCancel);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Logout hframe=new Logout();
				hframe.setVisible(true);
				
				
			}
		});
		btnOk.setForeground(new Color(0, 0, 128));
		btnOk.setBackground(new Color(30, 144, 255));
		btnOk.setBounds(204, 324, 66, 23);
		contentPane.add(btnOk);

	}
}
