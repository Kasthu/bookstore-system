package bookstore_system;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class Bookdata_connect {
	public static int save(int book_id, String book_name, String author_name, float price, int quantity
			) {
		Connection conn = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "keeran");
		} catch (Exception e) {
			{
				System.out.println(e);
			}

		}
		int status = 0;
		try {

			PreparedStatement ps = conn.prepareStatement(
					"insert into book_data(book_id,book_name,author_name,price,quantity) values(?,?,?,?,?)");

			ps.setInt(1, book_id);

			ps.setString(2, book_name);

			ps.setString(3, author_name);

			ps.setFloat(4,price);

			ps.setInt(5, quantity);

			

			
			status = ps.executeUpdate();
			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}
}
