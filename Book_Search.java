package bookstore_system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.MessageFormat;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class Book_Search extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField ta;
	private JLabel name;
	private JComboBox book_combo;
	//public JLabel lblTotalAmount;
	Connection conn = null;
	/**
	 * Launch the application.
	 */
	

    
   
	/*public void getSum() {
	int sum=0;
	for(int i=0;i<table.getRowCount();i++)
	{
		sum=sum+Integer.parseInt(table.getValueAt(i, 2).toString());
	}
	ta.setText(Integer.toString(sum));
}
	public int getSum() {
		int rowCount=table.getRowCount();
		int sum=0;
		for(int i=0;i<rowCount;i++) {
			sum=sum+Integer.parseInt(table.getValueAt(i, 2).toString());
		}
		return sum;
	}*/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Book_Search frame = new Book_Search();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	
	private void product() {
		try {
			String Book="SELECT * FROM book_data";
			Statement stmt = conn.createStatement();
			ResultSet set = stmt.executeQuery(Book);
			while(set.next())
			{
				book_combo.addItem(set.getString("book_id"));	
			}
		}catch(Exception e){
			
		}
		
	}
	
	private JTextField an;
	private JTextField pz;
	
	public Book_Search()  {
		try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "keeran");
		}catch(ClassNotFoundException|SQLException e1) {
			e1.printStackTrace();
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 715, 450);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("Search Book");
		lblNewLabel.setForeground(Color.GREEN);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		JLabel lblBookId = new JLabel("Select Book ID");
		lblBookId.setForeground(new Color(0, 250, 154));
		lblBookId.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JScrollPane scrollPane = new JScrollPane();
		
		JLabel lblTotalAmount = new JLabel("Total Amount");
		lblTotalAmount.setForeground(new Color(0, 250, 154));
		lblTotalAmount.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		ta = new JTextField();
		ta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//ta.setText(Integer.toString(getSum()));
			}
		});
		ta.setColumns(10);
		
		
		JButton btnadd = new JButton("Add");
		btnadd.setForeground(new Color(0, 0, 0));
		btnadd.setBackground(new Color(0, 255, 127));
		btnadd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DefaultTableModel model=(DefaultTableModel)table.getModel();
				model.addRow(new Object[] {name.getText(),an.getText(),pz.getText()});
				
				float total=0;
				for(int i=0;i<table.getRowCount();i++) {
					float amount=Float.parseFloat((String)table.getValueAt(i, 2));
							
					total+=amount;
				}
			ta.setText(String.valueOf(total));
			}
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Logout hframe=new Logout();
					hframe.setVisible(true);
			}
		});
		btnCancel.setForeground(new Color(0, 0, 0));
		btnCancel.setBackground(new Color(0, 255, 127));
		
		book_combo = new JComboBox();
		book_combo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				String fetch_row="SELECT * FROM book_data where book_id=? ";
				PreparedStatement stmt=conn.prepareCall(fetch_row);
				stmt.setString(1, (String)book_combo.getSelectedItem());
				ResultSet set=stmt.executeQuery();
				while(set.next())
				{
					//book_combo.addItem(set.getString("book_name"));	
					name.setText(set.getString("book_name"));
					an.setText(set.getString("author_name"));
					pz.setText(set.getString("price"));
					
				}
				}catch(Exception e2){
					
				}
			}
		});
		
		JLabel lblBookName = new JLabel("Book Name");
		lblBookName.setForeground(new Color(0, 250, 154));
		lblBookName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JLabel lblAuthorName = new JLabel("Author Name");
		lblAuthorName.setForeground(new Color(0, 250, 154));
		lblAuthorName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		an = new JTextField();
		an.setColumns(10);
		
		JLabel lblPrice = new JLabel("Price");
		lblPrice.setForeground(new Color(0, 250, 154));
		lblPrice.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		pz = new JTextField();
		pz.setColumns(10);
		
		product();
		
		JButton btnOk = new JButton("Conform");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Shopping_card sc=new Shopping_card();
				sc.setVisible(true);
			}
		});
		btnOk.setForeground(Color.BLACK);
		btnOk.setBackground(new Color(0, 255, 127));
		
		name = new JLabel("");
		name.setForeground(new Color(0, 250, 154));
		name.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		JButton btnPrint = new JButton("Print");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MessageFormat header=new MessageFormat("Purchase details");
				MessageFormat footer=new MessageFormat("page");
				try {
					table.print(JTable.PrintMode.FIT_WIDTH,header,footer);
				}catch(Exception e1) {
					JOptionPane.showInternalMessageDialog(null, "unable to print");
				}
				
			}
		});
		btnPrint.setBackground(new Color(0, 250, 154));
		
		JButton btnViewBookDetails = new JButton("View Book Details");
		btnViewBookDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Book_details hframe=new Book_details();
				hframe.setVisible(true);
			}
		});
		btnViewBookDetails.setForeground(Color.BLACK);
		btnViewBookDetails.setBackground(new Color(0, 255, 127));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblBookId, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
								.addComponent(book_combo, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
							.addGap(10)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblBookName, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
								.addComponent(name, GroupLayout.DEFAULT_SIZE, 205, Short.MAX_VALUE))
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(4)
									.addComponent(an, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(pz, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(14)
									.addComponent(lblAuthorName, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(lblPrice, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
									.addGap(33))))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblTotalAmount)
									.addGap(18)
									.addComponent(ta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(btnPrint, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
									.addGap(175)
									.addComponent(btnOk, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)))))
					.addGap(27))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(576, Short.MAX_VALUE)
					.addComponent(btnadd, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
					.addGap(46))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addGap(269)
					.addComponent(lblNewLabel)
					.addContainerGap(307, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnViewBookDetails, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(485, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblBookName, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblBookId)
						.addComponent(lblAuthorName, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPrice, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(book_combo, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(name, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(an, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(pz, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGap(75)
					.addComponent(btnadd)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
					.addGap(27)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTotalAmount)
						.addComponent(ta, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnOk)
						.addComponent(btnPrint)
						.addComponent(btnCancel))
					.addGap(18)
					.addComponent(btnViewBookDetails)
					.addGap(191))
		);
		
		
		
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				
			},
			new String[] {
				"Book_Name", "Author_Name", "Price"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		table.getColumnModel().getColumn(0).setPreferredWidth(110);
		table.getColumnModel().getColumn(1).setPreferredWidth(142);
		scrollPane.setViewportView(table);
		contentPane.setLayout(gl_contentPane);
		
	}
}
