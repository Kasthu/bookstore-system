package bookstore_system;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Home_Page extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home_Page frame = new Home_Page();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Home_Page() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 511, 363);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(220, 20, 60));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("");
		Image img=new ImageIcon(this.getClass().getResource("/MG_4451-b2-n8wamizklkn8wg4vn6uk6w9zn0nrjlvu9gq2mywrm2.jpg")) .getImage();
		label.setIcon(new ImageIcon(img));
		label.setBounds(0, 0, 500, 302);
		contentPane.add(label);
		
		JButton btnworker = new JButton("Worker");
		btnworker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Worker_login hframe=new Worker_login();
				hframe.setVisible(true);
				
				
			}
		});
		btnworker.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnworker.setBackground(new Color(169, 169, 169));
		btnworker.setForeground(new Color(255, 255, 255));
		btnworker.setBounds(0, 300, 243, 31);
		contentPane.add(btnworker);
		
		JButton btncustomer = new JButton("Customer");
		btncustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			Book_details hframe=new Book_details();
				hframe.setVisible(true);
			}
		});
		btncustomer.setFont(new Font("Tahoma", Font.BOLD, 12));
		btncustomer.setBackground(new Color(169, 169, 169));
		btncustomer.setForeground(new Color(255, 255, 255));
		btncustomer.setBounds(242, 300, 258, 31);
		contentPane.add(btncustomer);
		
		JLabel lblNewLabel = new JLabel("NJ Book Store");
		lblNewLabel.setForeground(new Color(0, 0, 255));
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 21));
		lblNewLabel.setBounds(158, 11, 327, 36);
		contentPane.add(lblNewLabel);
	}

}
