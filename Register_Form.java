package bookstore_system;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Window;

import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.event.ActionEvent;

public class Register_Form extends JFrame {

	private JPanel contentPane;
	private JTextField textField_fn;
	private JTextField textField_ln;
	private JTextField textField_un;
	private JTextField textField_addr;
	private JPasswordField passwordField_pw;
	private JTextField userid;
	//Connection conn = null;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register_Form frame = new Register_Form();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	

	public Register_Form() {
		/*try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore", "root", "keeran");
		}catch(ClassNotFoundException|SQLException e1) {
			e1.printStackTrace();
		}*/
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, -14, 347, 511);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 20, 147));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblRegister = new JLabel("Register");
		lblRegister.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblRegister.setForeground(new Color(0, 0, 139));
		lblRegister.setBounds(115, 11, 101, 31);
		contentPane.add(lblRegister);

		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setForeground(new Color(255, 255, 255));
		lblFirstName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblFirstName.setBounds(44, 93, 82, 14);
		contentPane.add(lblFirstName);

		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setForeground(Color.WHITE);
		lblLastName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblLastName.setBounds(44, 129, 82, 14);
		contentPane.add(lblLastName);

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setForeground(Color.WHITE);
		lblUsername.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUsername.setBounds(44, 167, 82, 14);
		contentPane.add(lblUsername);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPassword.setBounds(44, 206, 82, 14);
		contentPane.add(lblPassword);

		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setForeground(Color.WHITE);
		lblAddress.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAddress.setBounds(44, 254, 82, 14);
		contentPane.add(lblAddress);

		textField_fn = new JTextField();
		textField_fn.setBounds(180, 93, 126, 20);
		contentPane.add(textField_fn);
		textField_fn.setColumns(10);

		textField_ln = new JTextField();
		textField_ln.setColumns(10);
		textField_ln.setBounds(180, 129, 126, 20);
		contentPane.add(textField_ln);

		textField_un = new JTextField();
		textField_un.setColumns(10);
		textField_un.setBounds(180, 167, 126, 20);
		contentPane.add(textField_un);

		textField_addr = new JTextField();
		textField_addr.setColumns(10);
		textField_addr.setBounds(180, 254, 143, 80);
		contentPane.add(textField_addr);

		passwordField_pw = new JPasswordField();
		passwordField_pw.setBounds(180, 206, 126, 20);
		contentPane.add(passwordField_pw);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnCancel.setBackground(new Color(255, 0, 0));
		btnCancel.setForeground(new Color(255, 255, 255));
		btnCancel.setBounds(234, 399, 89, 23);
		contentPane.add(btnCancel);

		JButton btnRegister = new JButton("Register");
		btnRegister.setBackground(new Color(50, 205, 50));
		btnRegister.setForeground(new Color(255, 255, 255));
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent args0) {
				int id =Integer.parseInt(userid.getText()) ;				
				String fname = textField_fn.getText();
				String lname = textField_ln.getText();
				String uname = textField_un.getText();
				String pass = String.copyValueOf(passwordField_pw.getPassword());
				String address = textField_addr.getText();
				int i=Registerconnect.save(id, fname, lname, uname, pass,  address);
				if(i>0) {
					JOptionPane.showMessageDialog(Register_Form.this, "Register Successfully");
					dispose();
					Login li=new Login();
					
					li.Newscreen();
				}else{
					JOptionPane.showMessageDialog(Register_Form.this,"Sorry, unable to Register!");
				}
				

			}
		});

		btnRegister.setBounds(10, 399, 89, 23);
		contentPane.add(btnRegister);

		JLabel lblUserId = new JLabel("ID:");
		lblUserId.setForeground(Color.WHITE);
		lblUserId.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUserId.setBounds(44, 53, 82, 14);
		contentPane.add(lblUserId);

		userid = new JTextField();
		userid.setColumns(10);
		userid.setBounds(180, 53, 126, 20);
		contentPane.add(userid);

		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				userid.setText(null);
				textField_fn.setText(null);
				textField_ln.setText(null);
				textField_un.setText(null);
				textField_addr.setText(null);
				passwordField_pw.setText(null);
				
			}
		});
		btnClear.setForeground(Color.WHITE);
		btnClear.setBackground(Color.GRAY);
		btnClear.setBounds(127, 399, 89, 23);
		contentPane.add(btnClear);

		JButton btnlogin = new JButton("Click here to login");
		btnlogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dispose();
				Login jframe = new Login();
				jframe.Newscreen();

			}
		});
		btnlogin.setForeground(new Color(255, 255, 255));
		btnlogin.setBackground(new Color(255, 20, 147));
		btnlogin.setBounds(95, 438, 175, 23);
		contentPane.add(btnlogin);

	}

}
